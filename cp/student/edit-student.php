<?php 
$baseUrl = '../';
include_once $baseUrl.'layouts/header.php';

require_once 'process-edit.php';

if(isset($_GET['username'])){
  $username = $_GET['username'];
  $username = fixAllInput($username);
  $username = str_replace('\'', '', $username);
  $username = str_replace('\"', '', $username);
  $username = str_replace('\\', '', $username);
  $username = str_replace('\\\\', '', $username);
  $username = str_replace(' ', '', $username);
}
else{
  echo "Vui lòng chọn sinh viên cần sửa !";
  die();
}

$sql = "SELECT user.*,class.name as classname,subject.name as subjectname FROM class RIGHT JOIN user ON class.id = user.class_id LEFT JOIN subject ON user.subject_id = subject.id where username = '$username'";
$resultStudent = executeResult($sql,true);
if($resultStudent == null || $resultStudent =='')
{
  echo "Không có thông tin về sinh viên này !";
  die();
}

$sql = "select * from class";
$resultClass = executeResult($sql);

$sql = "select * from subject";
$resultSubject = executeResult($sql);


?>
<div class="main-container">
 <div class="main-header anim">Chỉnh sửa chi tiết sinh viên (MSV: <?=$resultStudent['username']?>) </div><div><a href="index.php"><button class="like" style="margin-top:0px;margin-bottom:-20px"> Quay lại</button></a></div>
 <br><br>
 <form class="form-group" method="POST" id="insertStudent" enctype="multipart/form-data">
   <div class="row">
    <div class="col-md-7">
      <div class="form-group">
        <label for="fullname">Họ tên:</label>
        <input type="text" value="<?=$resultStudent['fullname']?>" class="form-control" name="fullname" placeholder="Nhập họ tên" id="fullname">
      </div>
      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" value="<?=$resultStudent['email']?>" name="email" placeholder="Nhập email" id="email" required>
      </div>
      <div class="form-group">
        <label for="phonenumber">Số điện thoại:</label>
        <input type="number" class="form-control" value="<?=$resultStudent['phonenumber']?>" name="phonenumber" placeholder="Nhập số điện thoại" id="phonenumber" required>
      </div>
      <div class="form-group">
        <label for="address">Địa chỉ:</label>
        <input type="text" class="form-control" value="<?=$resultStudent['address']?>" name="address" placeholder="Nhập địa chỉ" id="address">
      </div>
      <div class="form-group">
        <label for="username">Mã sinh viên: (không thể thay đổi)</label>
        <input readonly type="number" class="form-control" value="<?=$resultStudent['username']?>" name="username" id="username">
      </div>
      <div class="form-group">
        <label for="password">Mật khẩu:</label>
        <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" id="password">
      </div>
      <div class="form-group">
        <label for="story">Tiểu sử</label>
        <input type="text" class="form-control" name="story" value="<?=$resultStudent['story']?>" placeholder="Nhập tiểu sử" id="story">
      </div>
      <div class="form-group">
        <label for="note">Ghi chú</label>
        <textarea class="form-control" name="note" id="note" rows="4"><?=$resultStudent['note']?></textarea>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label for="username">Ảnh đại diện:</label>
        <input type="file" class="form-group" name="thumbnail" accept="image/*" onchange="loadFile(event)">
        <img src="<?=fixUrl($resultStudent['thumbnail'])?>" style="width:100%;height:380px;border: 1px #d4d4d4 solid;padding: 7px;border-radius:50%;-moz-border-radius:50%;-webkit-border-radius:50%;" id="output">

      <script>
        var loadFile = function(event) {
          var output = document.getElementById('output');
          output.src = URL.createObjectURL(event.target.files[0]);
          output.onload = function() {
                      URL.revokeObjectURL(output.src) // free memory
                    }
                  };
        </script>

      </div>
      <div class="form-group">
        <label for="class">Lớp học</label>
        <select class="form-control" id="class" name="class" required>
          <option>---Chọn---</option>
          <?php 
          foreach ($resultClass as $item) {
            if($resultStudent['class_id'] == $item['id']){
              echo '<option value="'.$item['id'].'" selected>'.$item['name'].'</option>';
            }
            echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <label for="subject">Môn học:</label>
        <select class="form-control" id="subject" name="subject" required> 
          <option>---Chọn---</option>
          <?php 
          foreach ($resultSubject as $item) {
            if($resultStudent['subject_id'] == $item['id']){
              echo '<option value="'.$item['id'].'" selected>'.$item['name'].'</option>';
            }
            echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div> 
  <input type="submit" value="Submit" id="submitStudent" class="btn btn-primary">
</form>
</div>

<!-- <script type="text/javascript">
    $(function() {
      $('form').submit(function() {

        $.post(BASE_URL + API_AUTHEN, 
        {
          'action' : AUTHEN_EDIT_STUDENT,
          'username' : $('[name=username]').val(),
          'fullname' : $('[name=fullname]').val(),
          'email' : $('[name=email]').val(),
          'phonenumber' : $('[name=phonenumber]').val(),
          'address' : $('[name=address]').val(),
          'password' : $('[name=password]').val(),
          'class' : $('[name=class]').val(),
          'subject' : $('[name=subject]').val(),
          'story' : $('[name=story]').val(),
          'note' : $('[name=note]').val()
        }, 
        function(data) {
          obj = JSON.parse(data)

          if(obj.status == 1) {
            alert(obj.msg)
          } else {
            alert(obj.msg)
          }
        });

        return false
      });

    });
</script> -->

  <?php 
  include_once $baseUrl.'layouts/footer.php';
?>