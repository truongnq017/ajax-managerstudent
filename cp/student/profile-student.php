<?php 
$baseUrl = '../';
include_once $baseUrl.'layouts/header.php';

if(isset($_GET['username'])){
	$username = $_GET['username'];
	$username = fixAllInput($username);
	$username = str_replace('\'', '', $username);
	$username = str_replace('\"', '', $username);
	$username = str_replace('\\', '', $username);
	$username = str_replace('\\\\', '', $username);
	$username = str_replace(' ', '', $username);
}
else{
	echo "Vui lòng chọn sinh viên cần xem !";
	die();
}

$sql = "SELECT user.*,class.name as classname,subject.name as subjectname FROM class RIGHT JOIN user ON class.id = user.class_id LEFT JOIN subject ON user.subject_id = subject.id where username = '$username'";
$resultStudent = executeResult($sql,true);
if($resultStudent == null || $resultStudent =='')
{
	echo "Không có thông tin về sinh viên này !";
	die();
}
?>

<div class="main-container show">
	<div class="stream-area">
		<div class="video-stream">
			<img src="<?=fixUrl($resultStudent['thumbnail'])?>" style="width:100%">
			<div class="video-detail">
				<div class="video-content">
					<div class="video-p-wrapper anim" style="--delay: .1s">
						<div class="author-img__wrapper video-author video-p">
							<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
								<path d="M20 6L9 17l-5-5" />
							</svg>
							<img class="author-img" src="<?=fixUrl($resultStudent['thumbnail'])?>" />
						</div>
						<div class="video-p-detail">
							<div class="video-p-name"><?=$resultStudent['fullname']?></div>
							<div class="video-p-sub">
								<?php 
								if($resultStudent['deleted'] == 0){
									echo "Được thi";
								}else{
									echo "Cấm thi";
								}
							?></div>
						</div>
						<div class="button-wrapper">
							<a style="text-decoration: none" href="edit-student.php?username=<?=$resultStudent['username']?>">
								<button class="like">
									<svg viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
										<path d="M21.435 2.582a1.933 1.933 0 00-1.93-.503L3.408 6.759a1.92 1.92 0 00-1.384 1.522c-.142.75.355 1.704 1.003 2.102l5.033 3.094a1.304 1.304 0 001.61-.194l5.763-5.799a.734.734 0 011.06 0c.29.292.29.765 0 1.067l-5.773 5.8c-.428.43-.508 1.1-.193 1.62l3.075 5.083c.36.604.98.946 1.66.946.08 0 .17 0 .251-.01.78-.1 1.4-.634 1.63-1.39l4.773-16.075c.21-.685.02-1.43-.48-1.943z" />
									</svg>
									Chỉnh sửa
								</button>
							</a>
							<button class="like red"  onclick="banStudent(<?=$resultStudent['username']?>)">
								<svg viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M15.85 2.5c.63 0 1.26.09 1.86.29 3.69 1.2 5.02 5.25 3.91 8.79a12.728 12.728 0 01-3.01 4.81 38.456 38.456 0 01-6.33 4.96l-.25.15-.26-.16a38.093 38.093 0 01-6.37-4.96 12.933 12.933 0 01-3.01-4.8c-1.13-3.54.2-7.59 3.93-8.81.29-.1.59-.17.89-.21h.12c.28-.04.56-.06.84-.06h.11c.63.02 1.24.13 1.83.33h.06c.04.02.07.04.09.06.22.07.43.15.63.26l.38.17c.092.05.195.125.284.19.056.04.107.077.146.1l.05.03c.085.05.175.102.25.16a6.263 6.263 0 013.85-1.3zm2.66 7.2c.41-.01.76-.34.79-.76v-.12a3.3 3.3 0 00-2.11-3.16.8.8 0 00-1.01.5c-.14.42.08.88.5 1.03.64.24 1.07.87 1.07 1.57v.03a.86.86 0 00.19.62c.14.17.35.27.57.29z" />
								</svg>
								<?php 
								if($resultStudent['deleted'] == 0){
									echo "Cấm thi";
								}else{
									echo "Bỏ Cấm thi";
								}
								?>
							</button>
						</div>
					</div>
					<div class="video-p-title anim" style="--delay: .2s">Thông tin: 
					</div>
					<div class="video-p-subtitle anim" style="--delay: .3s"> 
						<p>Email : <?=$resultStudent['email']?></p>
						<p>SĐT : <?=$resultStudent['phonenumber']?></p>
						<p>Địa chỉ : <?=$resultStudent['address']?></p>
						<p>Lớp : <?=$resultStudent['classname']?></p>
						<p>Môn học : <?=$resultStudent['subjectname']?></p>
					</div>
					<div class="video-p-subtitle anim" style="--delay: .3s">Xếp hạng : Học sinh xuất xắc </div>
					<div class="video-p-subtitle anim" style="--delay: .4s">Hạnh kiểm : Tốt</div>
					<div class="video-p-subtitle anim" style="--delay: .4s">Ghi chú : Mải chơi</div>
					<hr>
					<div class="video-p-subtitle anim">
						<p style="color:#ea5f5f;font-weight:bold;font-size:18px">Bảng điểm</p>
						<div class="table-responsive">
							<table class="table table-bordered" style="color:white;--delay: .5s">
								<thead>
									<tr>
										<th>#</th>
										<th>Toán rời rạc</th>
										<th>Toán nâng cao</th>
										<th>Age</th>
										<th>City</th>
										<th>Country</th>
										<th>Sex</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
										<th>Example</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Kỳ 1</td>
										<td>2</td>
										<td>4</td>
										<td>6</td>
										<td>New York</td>
										<td>USA</td>
										<td>Female</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
									</tr>
									<tr>
										<td>Kỳ 2</td>
										<td>7</td>
										<td>4</td>
										<td>6</td>
										<td>New York</td>
										<td>USA</td>
										<td>Female</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
										<td>Yes</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="chat-stream">
			<div class="chat">
				<div class="chat-header anim">Thời khóa biểu<span><svg viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M14.212 7.762c0 2.644-2.163 4.763-4.863 4.763-2.698 0-4.863-2.119-4.863-4.763C4.486 5.12 6.651 3 9.35 3c2.7 0 4.863 2.119 4.863 4.762zM2 17.917c0-2.447 3.386-3.06 7.35-3.06 3.985 0 7.349.634 7.349 3.083 0 2.448-3.386 3.06-7.35 3.06C5.364 21 2 20.367 2 17.917zM16.173 7.85a6.368 6.368 0 01-1.137 3.646c-.075.107-.008.252.123.275.182.03.369.048.56.052 1.898.048 3.601-1.148 4.072-2.95.697-2.675-1.35-5.077-3.957-5.077a4.16 4.16 0 00-.818.082c-.036.008-.075.025-.095.055-.025.04-.007.09.019.124a6.414 6.414 0 011.233 3.793zm3.144 5.853c1.276.245 2.115.742 2.462 1.467a2.107 2.107 0 010 1.878c-.531 1.123-2.245 1.485-2.912 1.578a.207.207 0 01-.234-.232c.34-3.113-2.367-4.588-3.067-4.927-.03-.017-.036-.04-.034-.055.002-.01.015-.025.038-.028 1.515-.028 3.145.176 3.747.32z" />
				</svg>
				4 tiết/Tuần
			</span>
		</div>
		<div class="message-container">
			<div class="message anim" style="--delay: .1s">
				<div class="author-img__wrapper video-author video-p">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.unsplash.com/photo-1560941001-d4b52ad00ecc?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80" />
				</div>
				<div class="msg-wrapper">
					<div class="msg__name video-p-name"> Sáng Thứ 2</div>
					<div class="msg__content video-p-sub"> Toán rời rạc,Tin</div>
				</div>
			</div>
			<div class="message anim" style="--delay: .2s">
				<div class="author-img__wrapper video-author video-p">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.pexels.com/photos/2889942/pexels-photo-2889942.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
				</div>
				<div class="msg-wrapper">
					<div class="msg__name video-p-name offline"> Johny Wise</div>
					<div class="msg__content video-p-sub"> Suscipit eos atque voluptates labore</div>
				</div>
			</div>
			<div class="message anim" style="--delay: .3s">
				<div class="author-img__wrapper video-author video-p">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.unsplash.com/photo-1496345875659-11f7dd282d1d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mzl8fG1lbnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" />
				</div>
				<div class="msg-wrapper">
					<div class="msg__name video-p-name offline"> Budi Hakim</div>
					<div class="msg__content video-p-sub">Dicta quidem sunt adipisci</div>
				</div>
			</div>
			<div class="message anim" style="--delay: .4s">
				<div class="author-img__wrapper video-author video-p">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.pexels.com/photos/1870163/pexels-photo-1870163.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
				</div>
				<div class="msg-wrapper">
					<div class="msg__name video-p-name"> Thomas Hope</div>
					<div class="msg__content video-p-sub"> recusandae doloremque aperiam alias molestias</div>
				</div>
			</div>
			<div class="message anim" style="--delay: .5s">
				<div class="author-img__wrapper video-author video-p">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.pexels.com/photos/1680172/pexels-photo-1680172.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
				</div>
				<div class="msg-wrapper">
					<div class="msg__name video-p-name"> Gerard Will</div>
					<div class="msg__content video-p-sub">Dicta quidem sunt adipisci</div>
				</div>
			</div>
			<div class="message anim" style="--delay: .6s">
				<div class="author-img__wrapper video-author video-p">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.pexels.com/photos/2889942/pexels-photo-2889942.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500" />
				</div>
				<div class="msg-wrapper">
					<div class="msg__name video-p-name offline">Johny Wise</div>
					<div class="msg__content video-p-sub"> recusandae doloremque aperiam alias molestias</div>
				</div>
			</div>
		</div>
	</div>
	<div class="chat-vid__container">
		<div class="chat-vid__title anim" style="--delay: .3s">Các môn học đề xuất</div>
		<div class="chat-vid anim" style="--delay: .4s">
			<div class="chat-vid__wrapper">
				<img class="chat-vid__img" src="https://cdn.nohat.cc/thumb/f/720/3b55eddcfffa4e87897d.jpg" />
				<div class="chat-vid__content">
					<div class="chat-vid__name">Toán Cao Cấp</div>
					<div class="chat-vid__by">Jordan Wise</div>
					<div class="chat-vid__info">125.908 views <span class="seperate"></span>2 days ago</div>
				</div>
			</div>
		</div>
		<div class="chat-vid anim" style="--delay: .5s">
			<div class="chat-vid__wrapper">
				<img class="chat-vid__img" src="https://iamaround.it/wp-content/uploads/2015/02/pexels-photo-4663818.jpeg" />
				<div class="chat-vid__content">
					<div class="chat-vid__name">Giải thuật</div>
					<div class="chat-vid__by">Jordan Wise</div>
					<div class="chat-vid__info">125.908 views <span class="seperate"></span>2 days ago</div>
				</div>
			</div>
		</div>
		<div class="chat-vid__button anim" style="--delay: .6s">Xem tất cả môn học (32)</div>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	function banStudent(username){
		var option = confirm("Thay đổi Tình trạng Thi sinh viên này ?")
		if(!option){
			return
		}

		$.post(BASE_URL + API_AUTHEN,
		{
			'action': AUTHEN_BAN_STUDENT,
			'username': username
		},
		function(data) {
			obj = JSON.parse(data)

			if(obj.status == 1) {
				alert(obj.msg)
				location.reload();
			} else {
				alert(obj.msg)
				location.reload();
			}

		});
	}
</script>
<?php 
include_once $baseUrl.'layouts/header.php';
?>