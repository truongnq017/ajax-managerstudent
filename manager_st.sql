CREATE TABLE `role` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(20)
);

CREATE TABLE `class` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(20),
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `subject` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `user` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `role_id` int,
  `class_id` int,
  `subject_id` int,
  `username` varchar(20),
  `password` varchar(32),
  `fullname` varchar(255),
  `email` varchar(255),
  `phonenumber` varchar(20),
  `address` varchar(255),
  `attendance` int,
  `status` int default 0,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE tokens(
    id int PRIMARY KEY AUTO_INCREMENT,
    token varchar(32),
    user_id int,
    FOREIGN KEY (user_id) REFERENCES user(id),
    created_at datetime
    );

CREATE TABLE `log` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `note` varchar(255)
);

ALTER TABLE `user` ADD FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

ALTER TABLE `user` ADD FOREIGN KEY (`class_id`) REFERENCES `class` (`id`);

ALTER TABLE `user` ADD FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`);
