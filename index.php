<?php 
include_once './layouts/header.php';

?>
<!-- <div class="header">
	<div class="search-bar">
		<input type="text" placeholder="Search">
	</div>
</div> -->
<div class="main-container">
	<div class="main-header anim" style="color:#ff7551;">Welcome to Student Manager System</div>
	<br>
	<div class="main-blogs">
		<div class="main-blog anim" style="--delay: .1s">
			<div class="main-blog__title">How to do Basic Jumping and how to landing safely</div>
			<div class="main-blog__author">
				<div class="author-img__wrapper">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.unsplash.com/photo-1560941001-d4b52ad00ecc?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80" />
				</div>
				<div class="author-detail">
					<div class="author-name">Thomas Hope</div>
					<div class="author-info">53K views <span class="seperate"></span>2 weeks ago</div>
				</div>
			</div>
			<div class="main-blog__time">7 min</div>
		</div>
		<div class="main-blog anim" style="--delay: .2s">
			<div class="main-blog__title">Skateboard Tips You need to know</div>
			<div class="main-blog__author tips">
				<div class="main-blog__time">7 min</div>
				<div class="author-img__wrapper">
					<svg viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check">
						<path d="M20 6L9 17l-5-5" />
					</svg>
					<img class="author-img" src="https://images.unsplash.com/photo-1496345875659-11f7dd282d1d?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mzl8fG1lbnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60" />
				</div>
				<div class="author-detail">
					<div class="author-name">Tony Andrew</div>
					<div class="author-info">53K views <span></span>2 weeks ago</div>
				</div>
			</div>
		</div>
	</div>


</div>


<?php 
include_once 'layouts/footer.php';
?>