<?php 
session_start();
require_once '../utils/utility.php';
require_once '../database/dbhelper.php';

$action = getPOST('action');

switch ($action) {
	case 'insertStudent':
	insertStudent();
	break;

	case 'loginAdmin':
	loginAdmin();
	break;

/*	case 'editStudent':
	editStudent();
	break;*/

	case 'banStudent':
	banStudent();
	break;
}

//ban Student
function banStudent(){
	//lấy dữ liệu từ form
	$username = getPOST('username');
		//fix all sql injection :))
	$username = fixAllInput($username);
	$username = str_replace('\'', '', $username);
	$username = str_replace('\"', '', $username);
	$username = str_replace('\\', '', $username);
	$username = str_replace('\\\\', '', $username);
	$username = str_replace(' ', '', $username);

	$sql = "SELECT * FROM user WHERE username = '$username'";
	$checkUser = executeResult($sql,true);
	if($checkUser == null){
		$res = [
			"status" => 0,
			"msg" => 'Không tồn tại sinh viên này'
		];
		echo json_encode($res);
		die();
	}

	$fullname = $checkUser['fullname'];
	if ($checkUser['deleted'] == 0) {
		$sql = "UPDATE user SET deleted = 1 WHERE username = '$username'";
		execute($sql);
		$res = [
			"status" => 0,
			"msg" => 'Cấm thi sinh viên '.$fullname.' thành công'
		];
		echo json_encode($res);
		die();
	}else{
		$sql = "UPDATE user SET deleted = 0 WHERE username = '$username'";
		execute($sql);
		$res = [
			"status" => 1,
			"msg" => 'Bỏ cấm thi sinh viên '.$fullname.' thành công'
		];
		echo json_encode($res);
		die();

	}

}


//edit Student
function editStudent(){

}

//login admin
function loginAdmin(){
	$conn = mysqli_connect(HOST, USERNAME, PASSWORD, DATABASE);
	$username = getPOST('username');

	//fix all sql injection :))
	$username = fixAllInput($username);
	$username = str_replace('\'', '', $username);
	$username = str_replace('\"', '', $username);
	$username = str_replace('\\', '', $username);
	$username = str_replace('\\\\', '', $username);
	$username = str_replace(' ', '', $username);
	$username = mysqli_real_escape_string($conn,$username);

	$password = getPOST('password');
	$password = getPwdSecurity($password);

	if ($username == '' || $password == '') {
		$res = [
			"status" => 0,
			"msg"    => "Vui lòng nhập tài khoản hoặc mật khẩu !"
		];
		echo json_encode($res);
		die();
	}

	//check user
	$sql = "SELECT * FROM user where username = '$username' AND password = '$password' AND role_id = 1";
	$resultAdmin = executeResult($sql,true);

	//login success
	if($resultAdmin != null){
		//tạo ra token để lưu xuống hệ thống
		$token = getPwdSecurity($resultAdmin['username'].time());

		//lưu xuống cookie
		setcookie('token',$token,time() + 1*24*60*60, '/');

		//lưu session xuống
		$_SESSION['user'] = $resultAdmin;

		//lưu session vào database
		$user_id = $resultAdmin['id'];
		date_default_timezone_set("Asia/Ho_Chi_Minh");    
		$created_at = $updated_at = date('Y-m-d H:i:sa');

		$sql = "INSERT INTO tokens(token,user_id,created_at) VALUES ('$token','$user_id','$created_at')";
		execute($sql);

		$res = [
			"status" => 1,
			"msg" => "Đăng nhập thành công !"		
		];
		echo json_encode($res);
		die();

	}else{
		$res = [
			"status" => 0,
			"msg" => "Tài khoản hoặc mật khẩu không chính xác !"
		];
		echo json_encode($res);
		die();
	}

}


//insert student
function insertStudent(){


	//lấy dữ liệu từ form
	$fullname = getPOST('fullname');
	$email = getPOST('email');
	$username = getPOST('username');
	$password = getPOST('password');
	$class = getPOST('class');

	//xóa các kí tự đặc biệt các kiểu
	$fullname=fixAllInput($fullname);
	$email = fixAllInput($email);
	$username = fixAllInput($username);
	$class = fixAllInput($class);

	$sql = "select * from user where email = '$email'";
	$checkEmail = executeResult($sql,true);

	$sql = "select * from user where username = '$username'";
	$checkUser = executeResult($sql,true);

	if ($fullname == null || $fullname == '') {
		$res = [
			"status" => 0,
			"msg"    => "Vui lòng nhập đầy đủ họ tên !"
		];
		echo json_encode($res);
		die();
	}
	if ($email == null || $email == '') {
		$res = [
			"status" => 0,
			"msg"    => "Vui lòng nhập đầy đủ email !"
		];
		echo json_encode($res);
		die();
	}
	if ($checkEmail != null) {
		$res = [
			"status" => 0,
			"msg"    => "Email đã tồn tại trong hệ thống !"
		];
		echo json_encode($res);
		die();
	}
	if ($checkUser != null) {
		$res = [
			"status" => 0,
			"msg"    => "Mã sinh viên đã tồn tại trong hệ thống"
		];
		echo json_encode($res);
		die();
	}
	if ($email == null || $email == '') {
		$res = [
			"status" => 0,
			"msg"    => "Vui lòng nhập đầy đủ email !"
		];
		echo json_encode($res);
		die();
	}
	if ($class == null || $class == '') {
		$res = [
			"status" => 0,
			"msg"    => "Vui lòng nhập đầy đủ lớp học !"
		];
		echo json_encode($res);
		die();
	}
	if ($password != null || $password != '') {
		$password = getPwdSecurity($password);
	}else {
		$password = "24333dcb1d80a14dd663e32a9883c276";
	}
	//check Mã sinh viên	
	if (!preg_match("/^[0-9]+$/",$username) || $username == '') {
		$res = [
			"status" => 0,
			"msg"    => "Mã sinh viên chỉ được chứa số !"
		];
		echo json_encode($res);
		die();
	} 
	date_default_timezone_set("Asia/Ho_Chi_Minh");    
	$created_at = $updated_at = date('Y-m-d H:i:sa');

	//insert data
	$sql = "INSERT INTO user(role_id,class_id,username,password,fullname,email,phonenumber,address,attendance,status,created_at,updated_at,thumbnail) VALUES ('3','$class','$username','$password','$fullname','$email','','','0','0','$created_at','$updated_at','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSEZsIQZe9rXMk60E_lXVCr22CsDbzLKLm4wPEAUWr5gnHXx14PjSVsT0E6yH5ruRebl1M&usqp=CAU')";
	execute($sql);

	//trả về
	$res = [
		"status" => 1,
		"msg"    => "Cấp tài khoản sinh viên thành công"
	];
	echo json_encode($res);

}

?>