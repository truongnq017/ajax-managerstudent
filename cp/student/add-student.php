<?php 
$baseUrl = '../';
include_once $baseUrl.'layouts/header.php';


$sql = "select * from class";
$resultClass = executeResult($sql);

$sql = "select * from subject";
$resultSubject = executeResult($sql);
?>
<div class="main-container">
 <div class="main-header anim">Cấp nhanh tài khoản Sinh viên </div><div><a href="index.php"><button class="like" style="margin-top:0px;margin-bottom:-20px"> Quay lại</button></a></div>
 <br><br>
 <form class="form-group" method="POST" id="insertStudent">
   <div class="row">
    <div class="col-md-7">
      <div class="form-group">
        <label for="fullname">Họ tên:</label>
        <input type="text" class="form-control" name="fullname" placeholder="Nhập họ tên" id="fullname">
      </div>
      <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" class="form-control" name="email" placeholder="Nhập email" id="email" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label for="username">Mã sinh viên:</label>
        <input type="number" required class="form-control" name="username" placeholder="Nhập mã sinh viên" id="username">
      </div>
      <div class="form-group">
        <label for="password">Mật khẩu: (Bỏ trống mặc định là 123456)</label>
        <input type="text" class="form-control" name="password" placeholder="Nhập mật khẩu" id="password">
      </div>
      <div class="form-group">
        <label for="class">Lớp học</label>
        <select class="form-control" id="class" name="class" required>
          <option>---Chọn---</option>
          <?php 
          foreach ($resultClass as $item) {
            echo '<option value="'.$item['id'].'">'.$item['name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div> 
  <button type="submit" id="submitStudent" class="btn btn-primary">Submit</button>
</form>
</div>

<script type="text/javascript">
    $(function() {
      $('form').submit(function() {

        $.post(BASE_URL + API_AUTHEN, 
        {
          'action' : AUTHEN_INSERT_STUDENT,
          'fullname' : $('[name=fullname]').val(),
          'email' : $('[name=email]').val(),
          'username' : $('[name=username]').val(),
          'password' : $('[name=password]').val(),
          'class' : $('[name=class]').val()
        }, 
        function(data) {
          obj = JSON.parse(data)

          if(obj.status == 1) {
            alert(obj.msg)
            location.reload();
          } else {
            alert(obj.msg)
          }
        });

        return false
      });
    });
</script>

  <?php 
  include_once $baseUrl.'layouts/footer.php';
?>