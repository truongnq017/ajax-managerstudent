var BASE_URL = 'http://localhost:81/backend/ajax/process/';

// Authen API
var API_AUTHEN = '/process-ajax.php';

var AUTHEN_INSERT_STUDENT = 'insertStudent';
var AUTHEN_LOGIN_ADMIN = 'loginAdmin';
var AUTHEN_EDIT_STUDENT = 'editStudent';
var AUTHEN_BAN_STUDENT = 'banStudent';
