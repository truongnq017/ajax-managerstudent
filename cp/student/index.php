<?php 
$baseUrl = '../';
include_once $baseUrl.'layouts/header.php';

$sql = "select * from user where role_id like 3";
$resultStudent = executeResult($sql);
?>
<div class="main-container">
 <div class="main-header anim" style="">Danh sách sinh viên 
 </div>
 <div style="padding-bottom:15px;">
  <a href="add-student.php"><button class="like" style="float:left"> Thêm sinh viên</button></a>

  <div class="search-bar" style="float:right">
    <input type="text" placeholder="Search">
  </div>
</div>

<div class="videos">

  <?php 
  foreach ($resultStudent as $item) {
    echo '<div class="video anim">
    <div class="video-time">';
    if($item['star'] == 0){
      echo "Bình thường";
    }elseif($item['star'] == 1){
      echo "Không tiến bộ";
    }elseif($item['star'] == 2){
      echo "Có tiến bộ nhẹ";
    }elseif($item['star'] == 3){
      echo "Có tiến bộ tốt";
    }else{
      echo "Xuất xắc";
    }
    echo'</div>
    <div class="video-wrapper">

    <a href="profile-student.php?username='.$item['username'].'"><img src="'.fixUrl($item['thumbnail']).'" style="width:100%"></a>

    <div class="author-img__wrapper video-author">
    </div>
    </div>
    <div class="video-by">';
    if($item['deleted'] == 0){
      echo "Được thi";
    }else{
      echo "Cấm thi";
    }
    echo'</div>
    <a href="profile-student.php?username='.$item['username'].'"><div class="video-name">'.$item['fullname'].'</div></a>
    <div class="video-view">MSV : '.$item['username'].'</div>
    </div>';
  }
  ?>



</div>
</div>


<?php 
include_once $baseUrl.'layouts/footer.php';
?>
